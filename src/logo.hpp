#pragma once

#include "common.hpp"

#include <string>
#include <cstring>
#include <vector>
#include <memory>

#include <wx/file.h>
#include <wx/string.h>

#include "scan.hpp"

class Logo{
private:
  std::vector<LOGO_PIXEL> lgps;
  std::unique_ptr<Scan> scan;
  LOGO_FILE_HEADER fileHeader;
  LOGO_HEADER header{ {'\0'}, 0, 0, 0, 0, 0, 0, 0, 0 };
public:
  Logo(std::unique_ptr<Scan>&& scan, const wxString& name, short x, short y, short w, short h); // constructor
  void create();
  void output(const wxString& fname);
};
