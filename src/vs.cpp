#include "vs.hpp"

VS::VS() : api(nullptr), core(nullptr), script(nullptr){
  vsscript_init();
}

VS::~VS(){
  if (this->api != nullptr && this->core != nullptr && this->script != nullptr) this->close();
  vsscript_finalize();
}

void VS::open(const char* fname){
  if (vsscript_evaluateFile(&this->script, fname, efSetWorkingDir)){
    throw std::runtime_error(vsscript_getError(this->script));
  }
  this->core = vsscript_getCore(this->script);
  this->api = vsscript_getVSApi2(vsscript_getApiVersion());
}

void VS::close(){
  vsscript_freeScript(this->script);
  this->api = nullptr;
  this->core = nullptr;
  this->script = nullptr;
}

const VSAPI* VS::getApi(){
  return this->api;
}

VSCore* VS::getCore(){
  return this->core;
}

VSNodeRef* VS::getOutput() {
	return vsscript_getOutput(this->script, 0);
}

int VS::getVersion(){
  return 1;
}
