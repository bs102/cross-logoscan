#include "internal.hpp"

Internal::Internal(const char* name){
  this->pix = std::make_shared<std::vector<PIXEL_YC>>();
  this->vs = std::make_unique<VS>();
  this->vs->open(name);
  this->vsapi = this->vs->getApi();
  this->vscore = this->vs->getCore();
  this->node = this->vs->getOutput();
  const VSVideoInfo* vinfo = this->vsapi->getVideoInfo(this->node);
  this->frame = 0;
  this->id = vinfo->format->id;
  this->width = vinfo->width;
  this->height = vinfo->height;
  this->area = { 0, 0, this->width, this->height };
  this->numFrames = vinfo->numFrames;
  this->bitsPerSample = vinfo->format->bitsPerSample;
  const VSFrameRef* vframeSample = this->vsapi->getFrame(this->frame, this->node, nullptr, 0);
  if (vframeSample != nullptr){
    int err = 0;
    auto props = this->vsapi->getFramePropsRO(vframeSample);
    this->matrix = static_cast<int>(this->vsapi->propGetInt(props, "_Matrix", 0, nullptr));
    if (this->matrix == 0) this->matrix = static_cast<int>(this->vsapi->propGetInt(props, "_Primaries", 0, &err));
    if (err != 0 || this->matrix == 2) this->matrix = 1; // default (BT.709)
    this->vsapi->freeFrame(vframeSample);
  } else std::runtime_error("failed to get frame");
  if (this->id == pfYUV420P8 || this->id == pfYUV422P8 || this->id == pfYUV420P10 || this->id == pfYUV422P10 ||
    this->id == pfYUV420P12 || this->id == pfYUV422P12 || this->id == pfYUV420P16 || this->id == pfYUV422P16){
    this->convertToYUV444();
  }
}

Internal::~Internal(){
  this->vsapi->freeNode(this->node);
  this->vs = nullptr;
}

void Internal::convertFromRGB(int tid){
  const VSFrameRef* frame = this->vsapi->getFrame((tid != -1) ? this->threadContainer.at(tid).frame : this->frame, this->node, nullptr, 0);
  auto src_r = this->vsapi->getReadPtr(frame, 0);
  auto src_g = this->vsapi->getReadPtr(frame, 1);
  auto src_b = this->vsapi->getReadPtr(frame, 2);
  auto src_stride = this->vsapi->getStride(frame, 0);
  int stride_range = src_stride / this->width;
  int shift_bit = this->bitsPerSample + 4;
  for (int j = 0; j < this->area.height; ++j){
    for (int i = 0; i < this->area.width * stride_range; i += stride_range){
      int pos = (this->area.y + j) * src_stride + (this->area.x + i);
      std::uint16_t r = static_cast<uint16_t>(src_r[pos]),
                    g = static_cast<uint16_t>(src_g[pos]),
                    b = static_cast<uint16_t>(src_b[pos]);
      if (stride_range > 1){
        r |= (static_cast<uint16_t>(src_r[pos + 1]) << 8);
        g |= (static_cast<uint16_t>(src_g[pos + 1]) << 8);
        b |= (static_cast<uint16_t>(src_b[pos + 1]) << 8);
      }
      // ref. rgb2yuy2 plugin (http://auf.jpn.xxxxxxxx.jp/)
      auto dst = (tid != -1) ? this->threadContainer.at(tid).pix : this->pix;
      if (this->matrix == 1){ // BT.709
        dst->emplace_back(
          static_cast<short>((13987 * r + 47055 * g + 4750 * b + 2048) >> shift_bit),
          static_cast<short>((-7539 * r - 25359 * g + 32896 * b + 2048) >> shift_bit),
          static_cast<short>((32896 * r - 29881 * g - 3017 * b + 2048) >> shift_bit)
        );
      } else if (this->matrix == 6){ // BT.601
        dst->emplace_back(
          static_cast<short>((19666 * r + 38595 * g + 7532 * b + 2048) >> shift_bit),
          static_cast<short>((-11105 * r - 21792 * g + 32897 * b + 2048) >> shift_bit),
          static_cast<short>((32897 * r - 27525 * g - 5372 * b + 2048) >> shift_bit)
        );
      } else if (this->matrix == 9){ // BT.2020
        // I'm not sure if this expression is right...
        dst->emplace_back(
          static_cast<short>((17281 * r + 52402 * g + 3901 * b + 2048) >> shift_bit),
          static_cast<short>((-9185 * r - 23706 * g + 32891 * b + 2048) >> shift_bit),
          static_cast<short>((32891 * r - 30246 * g - 2073 * b + 2048) >> shift_bit)
        );
      } else{
        std::stringstream str;
        str << "unsupported color matrix (" << this->matrix << ")";
        throw std::out_of_range(str.str().data());
      }
    }
  }
  this->vsapi->freeFrame(frame);
}

// YUVxxx -> YUV444
void Internal::convertToYUV444(){
  auto stdp = this->vsapi->getPluginById("com.vapoursynth.resize", this->vscore);
  auto args = this->vsapi->createMap();
  this->vsapi->propSetNode(args, "clip", this->node, paReplace);
  if (this->bitsPerSample == 8) this->vsapi->propSetInt(args, "format", pfYUV444P8, paReplace);
  else if (this->bitsPerSample == 10) this->vsapi->propSetInt(args, "format", pfYUV444P10, paReplace);
  else if (this->bitsPerSample == 12) this->vsapi->propSetInt(args, "format", pfYUV444P12, paReplace);
  else if (this->bitsPerSample == 16) this->vsapi->propSetInt(args, "format", pfYUV444P16, paReplace);
  else throw std::range_error("unsupported bits");
  std::string mat_s;
  if (this->matrix == 1) mat_s = "709";
  else if (this->matrix == 6) mat_s = "170m";
  else if (this->matrix == 9) mat_s = "2020ncl";
  else throw std::range_error("unsupported color matrix");
  this->vsapi->propSetData(args, "matrix_in_s", mat_s.data(), sizeof(mat_s.data()), paReplace);
  auto ret = this->vsapi->invoke(stdp, "Bilinear", args);
  this->vsapi->freeMap(args);
  if (const char* err = this->vsapi->getError(ret)) throw std::runtime_error(err);
  this->vsapi->freeNode(this->node);
  this->node = this->vsapi->propGetNode(ret, "clip", 0, nullptr);
  this->vsapi->freeMap(ret);
}

// YUV444 (8, 10, 12, 16 bit) -> YC48
void Internal::convertFromYCbCr(int tid){
  const VSFrameRef* f = this->vsapi->getFrame((tid != -1) ? this->threadContainer.at(tid).frame : this->frame, this->node, nullptr, 0);
  auto src_y = this->vsapi->getReadPtr(f, 0);
  auto src_cb = this->vsapi->getReadPtr(f, 1);
  auto src_cr = this->vsapi->getReadPtr(f, 2);
  auto src_stride = this->vsapi->getStride(f, 0);
  int stride_range = src_stride / this->width;
  for (int j = 0; j < this->area.height; ++j){
    for (int i = 0; i < this->area.width * stride_range; i += stride_range){
      int pos = (this->area.y + j) * src_stride + (this->area.x + i);
      std::uint16_t y = static_cast<uint16_t>(src_y[pos]),
                    cb = static_cast<uint16_t>(src_cb[pos]),
                    cr = static_cast<uint16_t>(src_cr[pos]);
      if (stride_range > 1){
        y |= (static_cast<uint16_t>(src_y[pos + 1]) << 8);
        cb |= (static_cast<uint16_t>(src_cb[pos + 1]) << 8);
        cr |= (static_cast<uint16_t>(src_cr[pos + 1]) << 8);
      }
      short dst_y, dst_cb, dst_cr;
      if (this->bitsPerSample == 8){ // 8 bit
        dst_y = ((y * 1197) >> 6) - 299;
        dst_cb = ((cb - 128) * 4681 + 164) >> 8;
        dst_cr = ((cr - 128) * 4681 + 164) >> 8;
      } else if (this->bitsPerSample == 10){ // 10 bit
        dst_y = ((y * 1197) >> 8) - 299;
        dst_cb = ((cb - 512) * 4681 + 164) >> 10;
        dst_cr = ((cr - 512) * 4681 + 164) >> 10;
      } else if (this->bitsPerSample == 12){ // 12 bit
        dst_y = ((y * 1197) >> 10) - 299;
        dst_cb = ((cb - 2048) * 4682 + 164) >> 12;
        dst_cr = ((cr - 2048) * 4682 + 164) >> 12;
      } else if (this->bitsPerSample == 16){ // 16 bit
        dst_y = ((y * 1197) >> 14) - 299;
        dst_cb = ((cb - 32768) * 4682 + 164) >> 16;
        dst_cr = ((cr - 32768) * 4682 + 164) >> 16;
      }
      if (tid != -1) this->threadContainer.at(tid).pix->emplace_back(dst_y, dst_cb, dst_cr);
      else this->pix->emplace_back(dst_y, dst_cb, dst_cr);
    }
  }
  this->vsapi->freeFrame(f);
}

// YC48 -> RGB24 (for preview)
unsigned char* Internal::convertToRGB(int tid){
  unsigned char* out = new unsigned char[this->getWidth() * this->getHeight() * 3];
  int i = 0;
  auto pix_obj = (tid != -1) ? this->threadContainer.at(tid).pix : this->pix;
  for (auto it = pix_obj->begin(); it != pix_obj->end(); ++it, ++i){
    int dst_r = 0, dst_g = 0, dst_b = 0;
    if (this->matrix == 1){ // BT.709
      dst_r = (255 * it->y + 401 * it->cr + 2048) >> 12;
      dst_g = (255 * it->y - 48 * it->cb - 120 * it->cr + 2048) >> 12;
      dst_b = (255 * it->y + 473 * it->cb + 2048) >> 12;
    } else if (this->matrix == 6){ // BT.601
      dst_r = (255 * it->y + 358 * it->cr + 2048) >> 12;
      dst_g = (255 * it->y - 88 * it->cb - 182 * it->cr + 2048) >> 12;
      dst_b = (255 * it->y + 452 * it->cb + 2048) >> 12;
    } else if (this->matrix == 9){ // BT.2020
      dst_r = (255 * it->y + 480 * it->cr + 2048) >> 12;
      dst_g = (255 * it->y - 33 * it->cb - 186 * it->cr + 2048) >> 12;
      dst_b = (255 * it->y + 376 * it->cb + 2048) >> 12;
    }
    if (dst_r > 255) dst_r = 255;
    else if (dst_r < 0) dst_r = 0;
    if (dst_g > 255) dst_g = 255;
    else if (dst_g < 0) dst_g = 0;
    if (dst_b > 255) dst_b = 255;
    else if (dst_b < 0) dst_b = 0;
    out[i * 3] = dst_r;
    out[i * 3 + 1] = dst_g;
    out[i * 3 + 2] = dst_b;
  }
  return out;
}

void Internal::setFrame(int n, bool convert, int tid){
  if (tid != -1){
    auto t = this->threadContainer.begin() + tid;
    t->frame = n;
    t->pix->clear();
  } else{
    this->frame = n;
    this->pix->clear();
  }
  if (convert){
    if (this->id == pfRGB24 || this->id == pfRGB30){
      this->convertFromRGB(tid);
    } else if (this->id == pfYUV420P8 || this->id == pfYUV422P8 || this->id == pfYUV444P8 ||
      this->id == pfYUV420P10 || this->id == pfYUV422P10 || this->id == pfYUV444P10 ||
      this->id == pfYUV420P12 || this->id == pfYUV422P12 || this->id == pfYUV444P12 ||
      this->id == pfYUV420P16 || this->id == pfYUV422P16 || this->id == pfYUV444P16){
      this->convertFromYCbCr(tid);
    } else{
      throw std::out_of_range("unsupported format");
    }
  }
}

void Internal::setArea(int x, int y, int width, int height){
  this->area = { x, y, width, height };
}

std::shared_ptr<std::vector<PIXEL_YC>> Internal::getPixels(int tid){
  return (tid != -1) ? this->threadContainer.at(tid).pix : this->pix;
}

void Internal::reserveThreadSpace(int numThreads){
  this->threadContainer.resize(numThreads);
  int i = 0;
  for (auto &t : this->threadContainer){
    t.tid = i++;
    t.pix = std::make_shared<std::vector<PIXEL_YC>>();
  }
}

int Internal::getWidth(){
  return this->width;
}

int Internal::getHeight(){
  return this->height;
}

int Internal::getNumFrames(){
  return this->numFrames;
}
