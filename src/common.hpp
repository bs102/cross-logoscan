#pragma once

#include <stdexcept>
#include <string>

#include <VapourSynth.h>
#include <VSScript.h>
#include <VSHelper.h>

#include "logo.h"

#include "config.hpp"
