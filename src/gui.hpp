#pragma once

#include "common.hpp"

#include <memory>
#include <fstream>
#include <future>
#include <thread>
#include <chrono>
#include <mutex>
#include <atomic>
#include <exception>

#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif
#include <wx/overlay.h>
#include <wx/dcbuffer.h>
#include <wx/wfstream.h>

#include "internal.hpp"
#include "scan.hpp"
#include "logo.hpp"

class Logoscan : public wxApp{
public:
  virtual bool OnInit();
};

class ScanWindow : public wxFrame{
private:
#ifdef __WXMSW__
  wxTextCtrl* statusText;
  wxTextCtrl* percentageText;
  wxTextCtrl* elapsedText;
  wxTextCtrl* availableFramesText;
#else
  wxStaticText* statusText;
  wxStaticText* percentageText;
  wxStaticText* elapsedText;
  wxStaticText* availableFramesText;
#endif
  wxButton* abortButton;
  wxGauge* gauge;
  std::unique_ptr<Scan> scan;
  std::vector<int> scanFrameList;
  wxString fname;
  wxString logoName;
  int x;
  int y;
  int w;
  int h;
  std::atomic_int now;
  std::atomic_int available;
  std::mutex mut;
  std::exception_ptr ep;
  std::vector<std::thread> scanThreads;
  int scanThreadsNum;
  bool abortFlag;
  std::atomic_int abortReceivedFlag;
  int numFrames;
  void do_layout();
  void updateStatus(const wxString& str);
  void finishScan();
  void btnClicked(wxCommandEvent& event);
  void drawWindow();
  void runScan(wxIdleEvent& event);
public:
  ScanWindow(wxWindow* parent, wxWindowID id, int x, int y, int w, int h, int th, const wxString& fname, const wxString& logoName);
  ~ScanWindow();
};

class ScanInitWindow : public wxFrame{
private:
  wxTextCtrl * logoNameField;
  wxSlider* xSlider;
  wxSlider* ySlider;
  wxSlider* wSlider;
  wxSlider* hSlider;
  wxSlider* thSlider;
  wxStaticText* xSliderLabel;
  wxStaticText* ySliderLabel;
  wxStaticText* wSliderLabel;
  wxStaticText* hSliderLabel;
  wxStaticText* thSliderLabel;
  wxCheckBox* isOutputFrames;
  wxButton* scanButton;
  bool firstClick;
  void do_layout();
  void onShow(wxShowEvent& event);
  void update(wxScrollEvent& event);
  void updateSlider(wxScrollEvent& event);
  void updateSliderLabel();
  void btnClicked(wxCommandEvent& event);
  void btnScanClicked(wxCommandEvent& event);
public:
  ScanInitWindow(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_FRAME_STYLE);
  void setMinMax(int xmin, int xmax, int ymin, int ymax, int wmin, int wmax, int hmin, int hmax);
  void setValue(int x, int y, int w, int h);
};

class PreviewArea : public wxScrolledWindow{
private:
  int width;
  int height;
  wxBitmap * preview;
  wxDC* dc;
  wxRect rect;
  bool enableBind;
  void OnDraw(wxDC& dc);
  void onErase(wxEraseEvent& event) {};
  void mouseLeftClick(wxMouseEvent& event);
  void mouseDragging(wxMouseEvent& event);
  void mouseLeftDeclick(wxMouseEvent& event);
  void processRect();
public:
  PreviewArea(wxWindow* parent, wxWindowID id);
  ~PreviewArea();
  void setPreview(std::shared_ptr<Internal>& internal, int tid = -1);
  void clearPreview();
  void drawRect();
  wxRect* getRect();
  void changeMouseBind(bool enable = true);
};

class PreviewSlider : public wxSlider{
private:
  void update(wxCommandEvent& event);
public:
  PreviewSlider(wxWindow* parent, wxWindowID id);
  void createSlider(int value, int min, int max);
  void clearSlider();
};

class MainWindow : public wxFrame{
private:
  std::shared_ptr<Internal> internal;
  ScanInitWindow* scanInitWindow;
  PreviewArea* preview;
  PreviewSlider* slider;
  wxMenuBar* menubar;
  void registerMenu();
  void registerBind();
  void do_layout();
  void menuOpen(wxCommandEvent& event);
  void menuClose(wxCommandEvent& event);
  void menuAbout(wxCommandEvent& event);
  void onClose(wxCloseEvent& event);
public:
  MainWindow(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_FRAME_STYLE);
  std::shared_ptr<Internal>& getInternal();
  PreviewArea* getPreviewArea();
  ScanInitWindow* getScanInitWindow();
  PreviewSlider* getPreviewSlider();
  wxMenuBar* getMenuBar();
  void closeFile();
};
