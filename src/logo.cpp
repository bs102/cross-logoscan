#include "logo.hpp"

Logo::Logo(std::unique_ptr<Scan>&& scan, const wxString& name, short x, short y, short w, short h) : scan(std::move(scan)){
  std::strncpy(this->header.name, name.mb_str(), LOGO_MAX_NAME);
  this->header.x = x;
  this->header.y = y;
  this->header.h = h;
  this->header.w = w;
}

// create logo
void Logo::create(){
  this->scan->getLGPs(this->lgps);
}

// output logo
void Logo::output(const wxString& fname){
  std::strncpy(this->fileHeader.str, LOGO_FILE_HEADER_STR, LOGO_FILE_HEADER_STR_SIZE);
  this->fileHeader.logonum.l = SWAP_ENDIAN(1);
  wxFile f(fname, wxFile::write);
  if ((f.Write(&this->fileHeader, sizeof(this->fileHeader))) != sizeof(this->fileHeader))
    throw std::runtime_error("failed to write lgd (fheader)");
  size_t size = sizeof(this->header);
  if (f.Write(&this->header, size) != size)
    throw std::runtime_error("failed to write lgd (header)");
  for (auto &lgp : this->lgps){
    size_t pixsize = sizeof(lgp);
    f.SeekEnd();
    if (pixsize == 0 || f.Write(&lgp, pixsize) != pixsize) throw std::runtime_error("failed to write lgd (pixel)");
    size += pixsize;
  }
  if (size != LOGO_DATASIZE(&this->header)) throw std::runtime_error("failed to write lgd (unknown)");
}
