#include "scan.hpp"

static std::tuple<short, short, short> med_average(std::vector<short>& y, std::vector<short>& cb, std::vector<short>& cr){
  size_t size = y.size();
  if (size != cb.size() || size != cr.size()) throw std::invalid_argument("failed to get med_average");
  auto y_ave = std::accumulate(y.begin() + (size / 4), y.begin() + (size * 3 / 4), 0.0, [](double a, short x){
    return a + x;
  });
  auto cb_ave = std::accumulate(cb.begin() + (size / 4), cb.begin() + (size * 3 / 4), 0.0, [](double a, short x){
    return a + x;
  });
  auto cr_ave = std::accumulate(cr.begin() + (size / 4), cr.begin() + (size * 3 / 4), 0.0, [](double a, short x){
    return a + x;
  });
  y_ave = y_ave / (size / 2) + 0.5, cb_ave = cb_ave / (size / 2) + 0.5, cr_ave = cr_ave / (size / 2) + 0.5;
  return std::make_tuple(static_cast<short>(y_ave), static_cast<short>(cb_ave), static_cast<short>(cr_ave));
}

Scan::Scan(std::shared_ptr<Internal>& internal, int x, int y, int w, int h, int th) : internal(internal), x(x), y(y), w(w), h(h), th(th){
  this->pixels.resize(w * h);
}

template <typename T, typename S>
bool Scan::approximLine(T& x, S& y, double* a, double* b){
  double tmp;
  size_t n = x.size();
  if (n != y.size()) return false;
  double sum_x[3] = {0}, sum_y[3] = {0}, sum_x2[3] = {0}, sum_xy[3] = {0};
  auto it2 = y.begin();
  for (auto it = x.begin(); it != x.end(); ++it, ++it2){
    sum_x[0] += it->y;
    sum_x[1] += it->cb;
    sum_x[2] += it->cr;
    sum_y[0] += it2->y;
    sum_y[1] += it2->cb;
    sum_y[2] += it2->cr;
    sum_x2[0] += it->y * it->y;
    sum_x2[1] += it->cb * it->cb;
    sum_x2[2] += it->cr * it->cr;
    sum_xy[0] += it->y * it2->y;
    sum_xy[1] += it->cb * it2->cb;
    sum_xy[2] += it->cr * it2->cr;
  }
  for (int i = 0; i < 3; ++i){
    double sum_x_i = sum_x[i];
    double sum_y_i = sum_y[i];
    double sum_x2_i = sum_x2[i];
    double sum_xy_i = sum_xy[i];
    if ((tmp = static_cast<double>(n) * sum_x2_i - sum_x_i * sum_x_i) == 0.0) return false;
    a[i] = (static_cast<double>(n) * sum_xy_i - sum_x_i * sum_y_i) / tmp;
    b[i] = (sum_x2_i * sum_y_i - sum_x_i * sum_xy_i) / tmp;
  }
  return true;
}

template <class Iterator>
void Scan::getAB(Iterator& frame, double* A, double* B){
  double A1[3], B1[3], A2[3], B2[3];
  // Y
  if (!this->approximLine(frame.sampleBgs, frame.samples, A1, B1) || !this->approximLine(frame.samples, frame.sampleBgs, A2, B2))
    throw std::range_error("failed to get AB");
  for (int i = 0; i < 3; ++i){
    A[i] = (A1[i] + (1 / A2[i])) / 2;
    B[i] = (B1[i] + (-B2[i] / A2[i])) / 2;
  }
}

bool Scan::calBGColor(int tid){
  auto pix = this->internal->getPixels(tid);
  std::vector<short> tY, tCr, tCb;
  tY.reserve((this->w + this->h - 2) * 2);
  tCb.reserve((this->w + this->h - 2) * 2);
  tCr.reserve((this->w + this->h - 2) * 2);
  for (int j = 0; j < this->h; ++j){
    for (int i = 0; i < this->w; ++i){
      if (j == 0 || j == this->h - 1 || i == 0 || i == this->w-1){
        auto x = pix->at(i + j * this->w);
        tY.emplace_back(x.y);
        tCb.emplace_back(x.cb);
        tCr.emplace_back(x.cr);
      }
    }
  }
  if (std::sort(tY.begin(), tY.end()); std::abs(tY.front() - tY.back()) > this->th * 8){
    return false;
  } else if (std::sort(tCb.begin(), tCb.end()); std::abs(tCb.front() - tCb.back()) > this->th * 8){
    return false;
  } else if (std::sort(tCr.begin(), tCr.end()); std::abs(tCr.front() - tCr.back()) > this->th * 8){
    return false;
  }
  auto [y, cb, cr] = med_average(tY, tCb, tCr);
  for (int i = 0; i < this->w*this->h; ++i){
    if (tid != -1) this->threadContainer.at(tid).pixels.at(i).sampleBgs.emplace_back(y, cb, cr);
    else this->pixels.at(i).sampleBgs.emplace_back(y, cb, cr);
  }
  return true;
}

void Scan::addSample(int tid){
  auto pix = this->internal->getPixels(tid);
  int i = 0;
  for (auto it = pix->begin(); it != pix->end(); ++it, ++i){
    if (tid != -1) this->threadContainer.at(tid).pixels.at(i).samples.emplace_back(it->y, it->cb, it->cr);
    else this->pixels.at(i).samples.emplace_back(it->y, it->cb, it->cr);
  }
}

void Scan::getLGPs(std::vector<LOGO_PIXEL>& lgps){
  double A[3], B[3];
  short lgp[6];
  if (!this->threadContainer.empty()){
    for (auto &t : this->threadContainer){
      auto it = this->pixels.begin();
      for (auto &u : t.pixels){
        it->samples.insert(it->samples.begin(), u.samples.begin(), u.samples.end());
        it->sampleBgs.insert(it->sampleBgs.begin(), u.sampleBgs.begin(), u.sampleBgs.end());
        u.samples.clear();
        u.sampleBgs.clear();
        ++it;
      }
    }
  }
  for (auto& it : this->pixels){
    getAB(it, A, B);
    for (int i = 0; i < 3; ++i){
      if (A[i] == 1) lgp[i * 2 + 1] = lgp[i * 2] = 0;
      else{
        double tmp = B[i] / (1 - A[i]) + 0.5;
        if (std::abs(tmp) < 0x7FFF){
          lgp[i * 2 + 1] = static_cast<short>(tmp);
          tmp = static_cast<double>(1 - A[i]) * LOGO_MAX_DP + 0.5;
          if (std::abs(tmp) > 0x3FFF || static_cast<short>(tmp) == 0) lgp[i * 2 + 1] = lgp[i * 2] = 0;
          else lgp[i * 2] = static_cast<short>(tmp);
        } else lgp[i * 2 + 1] = lgp[i * 2] = 0;
      }
    }
    // dp_y, y, dp_cb, cb, dp_cr, cr
    lgps.emplace_back(LOGO_PIXEL{ lgp[0], lgp[1], lgp[2], lgp[3], lgp[4], lgp[5] });
  }
}

void Scan::reserveThreadSpace(int numThreads){
  this->threadContainer.resize(numThreads);
  for (auto &t : this->threadContainer){
    t.pixels.resize(this->w * this->h);
  }
}
