#pragma once

#include "common.hpp"

class VS{
private:
  const VSAPI* api;
  VSCore* core;
  VSScript* script;
public:
  VS();
  ~VS();
  void open(const char* fname);
  void close();
  const VSAPI* getApi();
  VSCore* getCore();
  VSNodeRef* getOutput();
  int getVersion();
};
