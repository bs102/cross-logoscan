#include "gui.hpp"

wxIMPLEMENT_APP(Logoscan);

bool Logoscan::OnInit(){
  wxInitAllImageHandlers();
  MainWindow* frame = new MainWindow(nullptr, wxID_ANY, wxEmptyString);
  SetTopWindow(frame);
  frame->Show();
  return true;
}
