#pragma once

#include "common.hpp"

#include <vector>
#include <memory>
#include <sstream>
#include <thread>

#include "vs.hpp"

// equivalent to AviUtl's YC48
struct PIXEL_YC{
  short y;
  short cb;
  short cr;
  PIXEL_YC(short y, short cb, short cr) : y(y), cb(cb), cr(cr) {}
};

class Internal{
  struct ThreadContainer{
    int tid;
    int frame;
    std::shared_ptr<std::vector<PIXEL_YC>> pix;
  };
  struct Area{
    int x;
    int y;
    int width;
    int height;
  };
private:
  std::unique_ptr<VS> vs;
  const VSAPI* vsapi;
  VSCore* vscore;
  VSNodeRef* node;
  int frame;
  int id;
  int width;
  int height;
  Area area;
  int numFrames;
  int bitsPerSample;
  int matrix;
  std::shared_ptr<std::vector<PIXEL_YC>> pix;
  std::vector<ThreadContainer> threadContainer;
  void convertToYUV444();
  void convertFromRGB(int tid = -1);
  void convertFromYCbCr(int tid = -1);
public:
  Internal(const char* name);
  ~Internal();
  unsigned char* convertToRGB(int tid = -1);
  void setFrame(int n, bool convert = true, int tid = -1);
  void setArea(int x, int y, int width, int height);
  std::shared_ptr<std::vector<PIXEL_YC>> getPixels(int tid = -1);
  void reserveThreadSpace(int numThreads);
  int getWidth();
  int getHeight();
  int getNumFrames();
};
