#pragma once

#include "common.hpp"

#include <vector>
#include <memory>
#include <numeric>
#include <type_traits>
#include <algorithm>
#include <functional>
#include <tuple>

#include "internal.hpp"

class Scan{
  struct Pixel{
    struct Sample{
      short y;
      short cb;
      short cr;
      Sample(short y, short cb, short cr) : y(y), cb(cb), cr(cr) {}
    };
    struct SampleBg{
      short y;
      short cb;
      short cr;
      SampleBg(short y, short cb, short cr) : y(y), cb(cb), cr(cr) {}
    };
    std::vector<Sample> samples;
    std::vector<SampleBg> sampleBgs;
  };
  struct ThreadScanContainer{
    int tid;
    std::vector<Pixel> pixels;
  };
private:
  int x;
  int y;
  int w;
  int h;
  int th;
  std::shared_ptr<Internal> internal;
  std::vector<Pixel> pixels;
  std::vector<ThreadScanContainer> threadContainer;
  template <typename T, typename S> bool approximLine(T& x, S& y, double* a, double* b);
  template <class Iterator> void getAB(Iterator& frame, double* A, double* B);
public:
  Scan(std::shared_ptr<Internal>& internal, int x, int y, int w, int h, int th);
  bool calBGColor(int tid = -1);
  void addSample(int tid = -1);
  void getLGPs(std::vector<LOGO_PIXEL>& lgps);
  void reserveThreadSpace(int numThreads);
};
