#include "gui.hpp"

MainWindow::MainWindow(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style) : wxFrame(parent, id, title, pos, size, wxDEFAULT_FRAME_STYLE){
  SetTitle(wxString(prog_name));
  SetSize(wxSize(1280, 720));
  this->SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_FRAMEBK));
  this->registerMenu();
  this->registerBind();
  this->preview = nullptr;
  this->internal = nullptr;
  this->scanInitWindow = new ScanInitWindow(this, wxID_ANY, wxEmptyString);
  this->do_layout();
}

void MainWindow::registerMenu(){
  this->menubar = new wxMenuBar();
  wxMenu* tmp_menu = new wxMenu();
  wxMenuItem* tmp_item;
  tmp_item = tmp_menu->Append(1, wxT("Open"), wxEmptyString);
  Bind(wxEVT_MENU, &MainWindow::menuOpen, this, tmp_item->GetId());
  tmp_item = tmp_menu->Append(2, wxT("Close"), wxEmptyString);
  Bind(wxEVT_MENU, &MainWindow::menuClose, this, tmp_item->GetId());
  tmp_menu->AppendSeparator();
  tmp_item = tmp_menu->Append(wxID_ANY, wxT("Exit"), wxEmptyString);
  Bind(wxEVT_MENU, [=](wxCommandEvent&){ Close(true); }, tmp_item->GetId());
  this->menubar->Append(tmp_menu, wxT("File"));
  tmp_menu = new wxMenu();
  tmp_item = tmp_menu->Append(wxID_ANY, wxT("About"), wxEmptyString);
  Bind(wxEVT_MENU, &MainWindow::menuAbout, this, tmp_item->GetId());
  this->menubar->Append(tmp_menu, wxT("Help"));
  SetMenuBar(this->menubar);
}

void MainWindow::registerBind(){
  Bind(wxEVT_CLOSE_WINDOW, &MainWindow::onClose, this);
}

void MainWindow::do_layout(){
  wxBoxSizer* sizer = new wxBoxSizer(wxVERTICAL);
  this->preview = new PreviewArea(this, wxID_ANY);
  sizer->Add(this->preview, 1, wxEXPAND, 0);
  this->slider = new PreviewSlider(this, wxID_ANY);
  sizer->Add(this->slider, 0, wxBOTTOM | wxEXPAND, 0);
  SetSizer(sizer);
  Layout();
}

void MainWindow::onClose(wxCloseEvent& event){
  Destroy();
}

void MainWindow::menuOpen(wxCommandEvent& event){
  wxFileDialog fd(this, _("Open VapourSynth file"), "", "", "VapourSynth files (*.vpy)|*.vpy", wxFD_OPEN | wxFD_FILE_MUST_EXIST);
  if (fd.ShowModal() == wxID_CANCEL) return;
  try{
    char buf[4096];
    strncpy(buf, static_cast<const char*>(fd.GetPath().mb_str(wxConvUTF8)), 4095);
    if (this->internal != nullptr) this->menuClose(event);
    this->internal = std::make_shared<Internal>(buf);
  } catch (const std::exception& e){
    wxMessageBox(wxString(e.what()), "Error", wxOK | wxICON_ERROR);
    this->menuClose(event);
    return;
  }
  this->slider->createSlider(0, 0, this->internal->getNumFrames());
  this->internal->setFrame(0);
  this->scanInitWindow->setMinMax(0, this->internal->getWidth(), 0, this->internal->getHeight(), 1, this->internal->getWidth(), 1, this->internal->getHeight());
  this->preview->setPreview(this->internal);
  this->preview->changeMouseBind();
}

void MainWindow::menuClose(wxCommandEvent& event){
  if (this->preview != nullptr){
    this->slider->clearSlider();
    this->preview->clearPreview();
    this->scanInitWindow->Hide();
  }
  this->internal = nullptr;
}

void MainWindow::menuAbout(wxCommandEvent& event){
  std::string title = "About ";
  title += prog_name;
  std::string message = prog_name;
  message += " ";
  message += prog_version;
  wxMessageBox(
    wxString(message),
    title, wxOK | wxICON_INFORMATION
  );
}

std::shared_ptr<Internal>& MainWindow::getInternal(){
  return this->internal;
}

ScanInitWindow* MainWindow::getScanInitWindow(){
  return this->scanInitWindow;
}

PreviewArea* MainWindow::getPreviewArea(){
  return this->preview;
}

PreviewSlider* MainWindow::getPreviewSlider(){
  return this->slider;
}

wxMenuBar* MainWindow::getMenuBar(){
  return this->menubar;
}

void MainWindow::closeFile(){
  wxCommandEvent a;
  this->menuClose(a);
}

PreviewArea::PreviewArea(wxWindow* parent, wxWindowID id) : wxScrolledWindow(parent, id) {
  this->SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_FRAMEBK));
  this->preview = nullptr;
  this->width = 0;
  this->height = 0;
  this->rect = wxRect(0, 0, 0, 0);
  this->enableBind = false;
  Bind(wxEVT_LEFT_UP, &PreviewArea::mouseLeftClick, this);
  Bind(wxEVT_MOTION, &PreviewArea::mouseDragging, this);
  Bind(wxEVT_LEFT_DOWN, &PreviewArea::mouseLeftDeclick, this);
  Bind(wxEVT_ERASE_BACKGROUND, &PreviewArea::onErase, this);
}

PreviewArea::~PreviewArea(){
  delete this->preview;
}

void PreviewArea::setPreview(std::shared_ptr<Internal>& internal, int tid) {
  this->width = internal->getWidth();
  this->height = internal->getHeight();
  SetScrollbars(1, 1, this->width, this->height, 0, 0);
  wxImage preview_image(this->width, this->height, internal->convertToRGB(tid), false);
  if (this->preview != nullptr) delete this->preview;
  this->preview = new wxBitmap(preview_image, -1);
}

void PreviewArea::clearPreview(){
  delete this->preview;
  this->preview = nullptr;
  this->rect = wxRect(0, 0, 0, 0);
  SetScrollbars(0, 0, 0, 0, 0, 0);
  wxClientDC dc(this);
  dc.Clear();
  this->changeMouseBind(false);
}

void PreviewArea::OnDraw(wxDC& dc){
  if (this->preview != nullptr){
    wxBufferedDC bdc(&dc, *this->preview, wxBUFFER_VIRTUAL_AREA);
  }
}

void PreviewArea::processRect(){
  MainWindow* parent = static_cast<MainWindow*>(this->GetParent());
  auto sw = parent->getScanInitWindow();
  sw->setValue(this->rect.GetX(), this->rect.GetY(), this->rect.GetWidth(), this->rect.GetHeight());
  if (!sw->IsShown()) sw->Show();
  else sw->Refresh();
  this->drawRect();
}

void PreviewArea::drawRect(){
  wxClientDC dc(this);
  wxOverlay ov;
  ov.Reset();
  wxDCOverlay odc(ov, &dc);
  odc.Clear();
  dc.SetPen(*wxRED_PEN);
  dc.SetBrush(*wxTRANSPARENT_BRUSH);
  dc.DrawRectangle(this->rect);
}

void PreviewArea::mouseLeftClick(wxMouseEvent& event){
  if (!this->enableBind) return;
  if (!this->rect.IsEmpty()){
    this->processRect();
  }
}

void PreviewArea::mouseDragging(wxMouseEvent& event){
  if (!this->enableBind) return;
  if (event.Dragging()){
    this->rect = wxRect(this->rect.GetTopLeft(), event.GetPosition());
    this->processRect();
    Refresh();
  }
}

void PreviewArea::mouseLeftDeclick(wxMouseEvent& event){
  if (!this->enableBind) return;
  this->rect = wxRect(event.GetPosition(), wxSize(0, 0));
  this->processRect();
}

wxRect* PreviewArea::getRect(){
  return &this->rect;
}

void PreviewArea::changeMouseBind(bool enable){
  this->enableBind = enable;
}

PreviewSlider::PreviewSlider(wxWindow* parent, wxWindowID id) : wxSlider(parent, id, 0, 0, 1, wxDefaultPosition, wxDefaultSize, wxSL_LABELS){
  this->Hide();
  Bind(wxEVT_SLIDER, &PreviewSlider::update, this);
}

void PreviewSlider::createSlider(int value, int min, int max){
  this->SetRange(min, max);
  this->SetValue(value);
  this->Show();
  this->GetParent()->Layout();
}

void PreviewSlider::clearSlider(){
  this->Hide();
  this->GetParent()->Layout();
}

void PreviewSlider::update(wxCommandEvent& event){
  MainWindow* parent = static_cast<MainWindow*>(this->GetParent());
  auto i = parent->getInternal();
  auto p = parent->getPreviewArea();
  i->setFrame(this->GetValue());
  p->setPreview(i);
  parent->Refresh();
}

ScanInitWindow::ScanInitWindow(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style) : wxFrame(parent, id, title, pos, size, wxDEFAULT_FRAME_STYLE){
  SetTitle(wxString(prog_name));
  SetSize(wxSize(300, 300));
  this->SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_FRAMEBK));
  this->EnableCloseButton(false);
  this->logoNameField = new wxTextCtrl(this, wxID_ANY);
  this->xSlider = new wxSlider(this, wxID_ANY, 0, 0, 1, wxDefaultPosition, wxDefaultSize);
  this->ySlider = new wxSlider(this, wxID_ANY, 0, 0, 1, wxDefaultPosition, wxDefaultSize);
  this->wSlider = new wxSlider(this, wxID_ANY, 0, 0, 1, wxDefaultPosition, wxDefaultSize);
  this->hSlider = new wxSlider(this, wxID_ANY, 0, 0, 1, wxDefaultPosition, wxDefaultSize);
  this->xSliderLabel = new wxStaticText(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(30, -1));
  this->ySliderLabel = new wxStaticText(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(30, -1));
  this->wSliderLabel = new wxStaticText(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(30, -1));
  this->hSliderLabel = new wxStaticText(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(30, -1));
  this->thSliderLabel = new wxStaticText(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(30, -1));
  this->thSlider = new wxSlider(this, wxID_ANY, 0, 0, 255, wxDefaultPosition, wxDefaultSize);
  this->isOutputFrames = new wxCheckBox(this, wxID_ANY, wxT("Output available frames list"));
  this->scanButton = new wxButton(this, 1000, wxT("Scan"));
  this->firstClick = true;
  this->do_layout();
  Bind(wxEVT_SCROLL_THUMBTRACK, &ScanInitWindow::updateSlider, this);
  Bind(wxEVT_SCROLL_THUMBRELEASE, &ScanInitWindow::update, this);
  Bind(wxEVT_BUTTON, &ScanInitWindow::btnClicked, this);
  Bind(wxEVT_SHOW, &ScanInitWindow::onShow, this);
  this->logoNameField->SetMaxLength(LOGO_MAX_NAME - 1);
}

void ScanInitWindow::do_layout(){
  wxBoxSizer* sizer_1 = new wxBoxSizer(wxVERTICAL);
  sizer_1->Add(this->logoNameField, 0, wxEXPAND, 0);
  wxBoxSizer* sizer_1g = new wxBoxSizer(wxHORIZONTAL);
  sizer_1g->Add(5, 0, 0);
  wxFlexGridSizer* grid_sizer_1 = new wxFlexGridSizer(0, 5, 5, 5);
  wxStaticText* label_1 = new wxStaticText(this, wxID_ANY, wxT("x"));
  wxButton* xup = new wxButton(this, 100, wxT(">"), wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT);
  wxButton* xdown = new wxButton(this, 101, wxT("<"), wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT);
  grid_sizer_1->Add(label_1, 0, wxALIGN_CENTER, 0);
  grid_sizer_1->Add(xdown, 0, wxALIGN_CENTER, 0);
  grid_sizer_1->Add(this->xSlider, 0, wxEXPAND, 0);
  grid_sizer_1->Add(xup, 0, wxALIGN_CENTER, 0);
  grid_sizer_1->Add(this->xSliderLabel, 0, wxALIGN_LEFT, 0);
  wxStaticText* label_2 = new wxStaticText(this, wxID_ANY, wxT("y"));
  wxButton* yup = new wxButton(this, 110, wxT(">"), wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT);
  wxButton* ydown = new wxButton(this, 111, wxT("<"), wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT);
  grid_sizer_1->Add(label_2, 0, wxALIGN_CENTER, 0);
  grid_sizer_1->Add(ydown, 0, wxALIGN_CENTER, 0);
  grid_sizer_1->Add(this->ySlider, 0, wxEXPAND, 0);
  grid_sizer_1->Add(yup, 0, wxALIGN_CENTER, 0);
  grid_sizer_1->Add(this->ySliderLabel, 0, wxALIGN_LEFT, 0);
  wxStaticText* label_3 = new wxStaticText(this, wxID_ANY, wxT("width"));
  wxButton* wup = new wxButton(this, 120, wxT(">"), wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT);
  wxButton* wdown = new wxButton(this, 121, wxT("<"), wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT);
  grid_sizer_1->Add(label_3, 0, wxALIGN_CENTER, 0);
  grid_sizer_1->Add(wdown, 0, wxALIGN_CENTER, 0);
  grid_sizer_1->Add(this->wSlider, 0, wxEXPAND, 0);
  grid_sizer_1->Add(wup, 0, wxALIGN_CENTER, 0);
  grid_sizer_1->Add(this->wSliderLabel, 0, wxALIGN_LEFT, 0);
  wxStaticText* label_4 = new wxStaticText(this, wxID_ANY, wxT("height"));
  wxButton* hup = new wxButton(this, 130, wxT(">"), wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT);
  wxButton* hdown = new wxButton(this, 131, wxT("<"), wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT);
  grid_sizer_1->Add(label_4, 0, wxALIGN_CENTER, 0);
  grid_sizer_1->Add(hdown, 0, wxALIGN_CENTER, 0);
  grid_sizer_1->Add(this->hSlider, 0, wxEXPAND, 0);
  grid_sizer_1->Add(hup, 0, wxALIGN_CENTER, 0);
  grid_sizer_1->Add(this->hSliderLabel, 0, wxALIGN_LEFT, 0);
  wxStaticText* label_5 = new wxStaticText(this, wxID_ANY, wxT("threshold"));
  wxButton* thup = new wxButton(this, 140, wxT(">"), wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT);
  wxButton* thdown = new wxButton(this, 141, wxT("<"), wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT);
  grid_sizer_1->Add(label_5, 0, wxALIGN_CENTER, 0);
  grid_sizer_1->Add(thdown, 0, wxALIGN_CENTER, 0);
  grid_sizer_1->Add(this->thSlider, 0, wxEXPAND, 0);
  grid_sizer_1->Add(thup, 0, wxALIGN_CENTER, 0);
  grid_sizer_1->Add(this->thSliderLabel, 0, wxALIGN_LEFT, 0);
  grid_sizer_1->AddGrowableCol(2);
  sizer_1g->Add(grid_sizer_1, 1, wxEXPAND, 0);
  sizer_1g->Add(5, 0, 0);
  sizer_1->Add(sizer_1g, 0, wxEXPAND, 0);
  sizer_1->Add(this->isOutputFrames, 0, 0, 0);
  sizer_1->Add(this->scanButton, 0, wxEXPAND, 0);
  SetSizer(sizer_1);
  sizer_1->Fit(this);
  Layout();
}

void ScanInitWindow::onShow(wxShowEvent& event){
  auto internal = static_cast<MainWindow*>(this->GetParent())->getInternal();
  this->logoNameField->SetValue(wxString::Format(wxT(" %ix%i"), internal->getWidth(), internal->getHeight()));
}

void ScanInitWindow::setMinMax(int xmin, int xmax, int ymin, int ymax, int wmin, int wmax, int hmin, int hmax){
  this->xSlider->SetRange(xmin, xmax);
  this->ySlider->SetRange(ymin, ymax);
  this->wSlider->SetRange(wmin, wmax);
  this->hSlider->SetRange(hmin, hmax);
}

void ScanInitWindow::setValue(int x, int y, int w, int h){
  int cx, cy;
  MainWindow* parent = static_cast<MainWindow*>(this->GetParent());
  auto p = parent->getPreviewArea();
  p->CalcUnscrolledPosition(x, y, &cx, &cy);
  this->xSlider->SetValue(cx);
  this->ySlider->SetValue(cy);
  this->wSlider->SetValue(w);
  this->hSlider->SetValue(h);
  this->updateSliderLabel();
}

void ScanInitWindow::update(wxScrollEvent& event){
  MainWindow* parent = static_cast<MainWindow*>(this->GetParent());
  auto p = parent->getPreviewArea();
  auto rect = p->getRect();
  rect->SetPosition(p->CalcScrolledPosition(wxPoint(this->xSlider->GetValue(), this->ySlider->GetValue())));
  rect->SetSize(wxSize(this->wSlider->GetValue(), this->hSlider->GetValue()));
  p->drawRect();
  this->updateSliderLabel();
}

void ScanInitWindow::updateSlider(wxScrollEvent& event){
  this->update(event);
  static_cast<MainWindow*>(this->GetParent())->getPreviewArea()->Refresh();
}

void ScanInitWindow::updateSliderLabel(){
  this->xSliderLabel->SetLabel(wxString::Format(wxT("%i"), this->xSlider->GetValue()));
  this->ySliderLabel->SetLabel(wxString::Format(wxT("%i"), this->ySlider->GetValue()));
  this->wSliderLabel->SetLabel(wxString::Format(wxT("%i"), this->wSlider->GetValue()));
  this->hSliderLabel->SetLabel(wxString::Format(wxT("%i"), this->hSlider->GetValue()));
  this->thSliderLabel->SetLabel(wxString::Format(wxT("%i"), this->thSlider->GetValue()));
}

void ScanInitWindow::btnClicked(wxCommandEvent& event){
  int val = event.GetId();
  if (val == 1000){
    try{
      this->btnScanClicked(event);
    } catch (const std::exception& e){
      wxMessageBox(wxString(e.what()), "Error", wxOK | wxICON_ERROR);
    }
    return;
  }
  int offset = (val % 2 == 1) ? -1 : 1;
  int mode = (offset < 0) ? (val - 1) % 100 : val % 100;
  switch (mode){
  case 0:
    this->xSlider->SetValue(this->xSlider->GetValue() + offset);
    break;
  case 10:
    this->ySlider->SetValue(this->ySlider->GetValue() + offset);
    break;
  case 20:
    this->wSlider->SetValue(this->wSlider->GetValue() + offset);
    break;
  case 30:
    this->hSlider->SetValue(this->hSlider->GetValue() + offset);
    break;
  case 40:
    this->thSlider->SetValue(this->thSlider->GetValue() + offset);
  }
  auto parent = static_cast<MainWindow*>(this->GetParent());
  auto pre = parent->getPreviewArea();
  auto internal = parent->getInternal();
  wxClientDC dc(pre);
  wxImage preview_image(internal->getWidth(), internal->getHeight(), internal->convertToRGB(), false);
  int cx, cy;
  pre->GetClientSize(&cx, &cy);
  wxImage new_image;
  if (cx < internal->getWidth() || cy < internal->getHeight()){
    cx = std::min(cx, internal->getWidth());
    cy = std::min(cy, internal->getHeight());
    new_image = preview_image.GetSubImage(wxRect(pre->GetViewStart(), wxSize(cx, cy)));
  } else{
    new_image = preview_image;
  }
  wxBitmap bmp(new_image);
  dc.DrawBitmap(bmp, 0, false);
  wxScrollEvent a;
  this->update(a);
}

void ScanInitWindow::btnScanClicked(wxCommandEvent& event){
  auto parent = static_cast<MainWindow*>(this->GetParent());
  auto internal = parent->getInternal();
  int x = this->xSlider->GetValue(), y = this->ySlider->GetValue(), w = this->wSlider->GetValue(), h = this->hSlider->GetValue(), th = this->thSlider->GetValue();
  if (x + w > internal->getWidth() || y + h > internal->getHeight()) throw std::out_of_range("invalid range");
  if (this->logoNameField->IsEmpty()) throw std::out_of_range("no logoname");

  // save frame log
  wxString fname;
  if (this->isOutputFrames->IsChecked()){
    wxFileDialog fd(this, _("Save frame list"), "", "", "TXT files (*.txt)|*.txt", wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
    if (fd.ShowModal() != wxID_CANCEL) fname << fd.GetPath();
  }

  // prepare
  parent->getPreviewSlider()->clearSlider();
  internal->setArea(x, y, w, h);
  internal->setFrame(0);
  parent->getPreviewArea()->setPreview(internal);
  parent->getPreviewArea()->changeMouseBind(false);
  auto menu = parent->getMenuBar()->GetMenu(0);
  menu->Enable(1, false);
  menu->Enable(2, false);

  // show ScanWindow
  auto scanWindow = new ScanWindow(static_cast<wxWindow*>(parent), wxID_ANY, x, y, w, h, th, fname, this->logoNameField->GetValue());
  scanWindow->Show();
  this->Hide();
}

ScanWindow::ScanWindow(wxWindow* parent, wxWindowID id, int x, int y, int w, int h, int th, const wxString& fname, const wxString& logoName) : wxFrame(parent, id, wxEmptyString), x(x), y(y), w(w), h(h), fname(fname), logoName(logoName){
  auto syscolor = wxSystemSettings::GetColour(wxSYS_COLOUR_FRAMEBK);
  auto syscolor_w = wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT);
  this->SetBackgroundColour(syscolor);
  this->EnableCloseButton(false);
  this->scan = std::make_unique<Scan>(static_cast<MainWindow*>(parent)->getInternal(), x, y, w, h, th);
  this->abortFlag = false;
  this->abortReceivedFlag = 0;
  this->now = 0;
  this->available = 0;
#ifdef __WXMSW__
  // use wxTextCtrl instead of wxStaticText due to flicker
  this->statusText = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(150, -1), wxTE_READONLY | wxBORDER_NONE);
  this->percentageText = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(30, -1), wxTE_READONLY | wxBORDER_NONE);
  this->elapsedText = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY | wxBORDER_NONE);
  this->availableFramesText = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(150, -1), wxTE_READONLY | wxBORDER_NONE);
  this->statusText->SetDefaultStyle(wxTextAttr(syscolor_w, syscolor));
  this->percentageText->SetDefaultStyle(wxTextAttr(syscolor_w, syscolor));
  this->elapsedText->SetDefaultStyle(wxTextAttr(syscolor_w, syscolor));
  this->availableFramesText->SetDefaultStyle(wxTextAttr(syscolor_w, syscolor));
#else
  this->statusText = new wxStaticText(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(150, -1));
  this->percentageText = new wxStaticText(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(30, -1));
  this->elapsedText = new wxStaticText(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize);
  this->availableFramesText = new wxStaticText(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(150, -1));
#endif
  this->gauge = new wxGauge(this, wxID_ANY, 10, wxDefaultPosition, wxSize(-1, 20));
  this->abortButton = new wxButton(this, 1, wxT("Abort"));
  this->scanThreadsNum = std::max(1, static_cast<int>(std::thread::hardware_concurrency()));
  this->numFrames = static_cast<MainWindow*>(this->GetParent())->getInternal()->getNumFrames();
  this->gauge->SetRange(this->numFrames);
  this->do_layout();
  Bind(wxEVT_BUTTON, &ScanWindow::btnClicked, this);
  Bind(wxEVT_IDLE, &ScanWindow::runScan, this);
  Bind(wxEVT_SHOW, [&](wxShowEvent&){ this->updateStatus(wxString::Format(wxT("Scanning... @ %i thread%s"), this->scanThreadsNum, (this->scanThreadsNum>1) ? "s" : "")); });
}

ScanWindow::~ScanWindow(){
  auto parent = static_cast<MainWindow*>(this->GetParent());
  auto menu = parent->getMenuBar()->GetMenu(0);
  menu->Enable(1, true);
  menu->Enable(2, true);
  parent->closeFile();
}

void ScanWindow::do_layout(){
  wxBoxSizer* sizer_1 = new wxBoxSizer(wxVERTICAL);
  wxGridSizer* grid_sizer_1 = new wxGridSizer(0, 2, 0, 0);
  wxGridSizer* grid_sizer_2 = new wxGridSizer(0, 3, 0, 0);
  grid_sizer_1->Add(this->statusText, 0, 0, 0);
  grid_sizer_1->Add(this->percentageText, 0, 0, 0);
  sizer_1->Add(grid_sizer_1, 1, wxEXPAND, 0);
  sizer_1->Add(this->gauge, 0, wxEXPAND, 0);
  sizer_1->Add(4, 0, 0);
  sizer_1->Add(this->elapsedText, 0, 0, 0);
  sizer_1->Add(4, 0, 0);
  grid_sizer_2->Add(this->availableFramesText, 0, wxALIGN_CENTER_VERTICAL, 0);
  grid_sizer_2->Add(1, 0, 0);
  grid_sizer_2->Add(this->abortButton, 0, wxALIGN_RIGHT, 0);
  sizer_1->Add(grid_sizer_2, 1, wxEXPAND, 0);
  SetSizer(sizer_1);
  sizer_1->Fit(this);
  Layout();
}

void ScanWindow::updateStatus(const wxString& str){
  SetTitle(str);
  this->statusText->SetLabel(str);
}

void ScanWindow::drawWindow(){
  this->gauge->SetValue(this->now);
  this->percentageText->SetLabel(wxString::Format(wxT("%i %%"), static_cast<int>((this->now / static_cast<double>(this->numFrames)) * 100), this->scanThreadsNum));
  this->elapsedText->SetLabel(wxString::Format(wxT("%i / %i"), this->now.load(), this->numFrames));
  this->availableFramesText->SetLabel(wxString::Format(wxT("Available frames: %i"), this->available.load()));
}

void ScanWindow::runScan(wxIdleEvent& event){
  if (!this->abortButton->IsThisEnabled()) return;
  try{
    if (this->scanThreads.empty()){
      static_cast<MainWindow*>(this->GetParent())->getInternal()->reserveThreadSpace(this->scanThreadsNum);
      this->scan->reserveThreadSpace(this->scanThreadsNum);
      for (int i = 0; i < this->scanThreadsNum; ++i){
        this->scanThreads.emplace_back([&](int tid){
          try{
            auto parent = static_cast<MainWindow*>(this->GetParent());
            auto internal = parent->getInternal();
            while (this->now < this->numFrames){
              int n = this->now++;
              internal->setFrame(n, true, tid);
              if (this->scan->calBGColor(tid)){
                ++this->available;
                this->scan->addSample(tid);
                /*
                {
                  std::lock_guard<std::mutex> lock(this->mut);
                  parent->getPreviewArea()->setPreview(internal, tid);
                  parent->getPreviewArea()->Refresh();
                }
                */
                this->scanFrameList.emplace_back(n);
              }
              if (this->abortFlag){
                ++this->abortReceivedFlag;
                return;
              }
            }
            ++this->abortReceivedFlag;
          } catch (...){
            this->ep = std::current_exception();
          }
        }, i);
        this->scanThreads.back().detach();
      }
    }
    if (this->ep){
      std::rethrow_exception(this->ep);
    }
    this->drawWindow();
    if (this->abortReceivedFlag == this->scanThreadsNum){
      this->finishScan();
      return;
    }
    event.RequestMore(true);
  } catch (const std::exception& e){
    wxMessageBox(wxString(e.what()), "Error", wxOK | wxICON_ERROR);
    this->Close();
  }
}

void ScanWindow::finishScan(){
  this->updateStatus(wxT("Creating..."));
  this->abortButton->Enable(false);
  if (this->available <= 1) throw std::out_of_range("no sample");

  // save logo
  wxString logoFileName;
  wxFileDialog fd(this, _("Save logo data"), "", "", "Logo data files (*.lgd)|*.lgd", wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
  if (fd.ShowModal() != wxID_CANCEL) logoFileName << fd.GetPath();
  else {
    this->Close();
    return;
  }

  Logo logo(std::move(this->scan), this->logoName, this->x, this->y, this->w, this->h);
  logo.create();
  logo.output(logoFileName);
  if (!this->fname.IsEmpty()){
    std::sort(this->scanFrameList.begin(), this->scanFrameList.end());
    std::ofstream ofs(this->fname.mb_str());
    if (!ofs) throw std::runtime_error("failed to write frame list");
    for (auto &p : this->scanFrameList){
      ofs << p << std::endl;
    }
  }
  this->Close();
}

void ScanWindow::btnClicked(wxCommandEvent& event){
  if (event.GetId() == 1){
    this->abortFlag = true;
  }
}
